extern crate proc_macro;

use syn;

pub fn extract_attribute(attrs: &[syn::Attribute], name: &str) -> String {
  attrs.iter()
    .filter_map(|attr| attr.parse_meta().ok())
    .filter(|m| m.path().is_ident(name))
    .filter_map(|m| match m {
      syn::Meta::NameValue(name_value) => Some(name_value.lit),
      _ => None
    })
    .filter_map(|value| match value {
      syn::Lit::Str(s) => Some(s.value().to_string()),
      _ => None,
    })
    .collect::<Vec<String>>()
    .pop()
    .expect(&format!("Expected macro attribute of the form '{} = \"...\"'", name))
}

