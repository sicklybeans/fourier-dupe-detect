mod dataset_builder;
mod runner;
mod runner_plot;

fn main() {
  // dataset_builder::run("/data/content/testing");
  runner::train().unwrap();
}
