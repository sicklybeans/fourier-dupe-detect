use plotters::prelude::*;

pub fn plot_weights<T>(outdir: &str, fname: &str, inputs: &[(Vec<f64>, bool, T)], weights: &[f64], bias: f64) {
  let output_filename = &format!("{}/{}", outdir, fname);
  let mut root = BitMapBackend::new(&output_filename, (640, 480)).into_drawing_area();
  root.fill(&WHITE).unwrap();
  root = root.titled("Learning results", ("Noto Sans", 40)).unwrap();

  let mut pos_values: Vec<(f64, f64)> = Vec::new();
  let mut neg_values: Vec<(f64, f64)> = Vec::new();
  for (value, d, _) in inputs.iter() {
    if *d {
      pos_values.push((value[0], value[1]));
    } else {
      neg_values.push((value[0], value[1]));
    }
  }

  let xrange = 0.0f64..20.0;
  let yrange = 0.0f64..20.0;

  let mut chart = ChartBuilder::on(&root)
    .x_label_area_size(35)
    .y_label_area_size(40)
    .margin(10)
    .build_cartesian_2d(xrange, yrange)
    .unwrap();

  chart.configure_mesh()
    .x_desc("k1")
    .y_desc("k2")
    .axis_desc_style(("sans-serif", 15))
    .draw()
    .unwrap();

  let slope = - weights[0] / weights[1];
  let inter = - bias / weights[1];
  // let slope = 1.0 / 0.3;

  chart.draw_series(LineSeries::new(
    (0..1000)
      .map(|x| ((x as f64) / 20.0))
      .map(|x| (x as f64, slope * (x as f64) + inter)),
    &BLUE)).unwrap();

  chart.draw_series(PointSeries::of_element(pos_values, 1, ShapeStyle::from(&BLUE).filled(),
    &|coord, size, style| { Circle::new(coord, size, style) }))
    .unwrap();
  chart.draw_series(PointSeries::of_element(neg_values, 1, ShapeStyle::from(&RED).filled(),
    &|coord, size, style| { Circle::new(coord, size, style) }))
    .unwrap();
}

#[allow(dead_code)]
pub fn plot_results(outdir: &str, fname: &str, results: &[bool], window: usize) {
  let output_filename = &format!("{}/{}", outdir, fname);
  let mut root = BitMapBackend::new(&output_filename, (640, 480)).into_drawing_area();
  root.fill(&WHITE).unwrap();
  root = root.titled("Learning results", ("Noto Sans", 40)).unwrap();
  let range = 0.0..(results.len() as f64 + 0.5);

  let mut data: Vec<(f64, f64)> = Vec::new();
  for i in window..(results.len() - window) {
    let value = results[i-window..i+window].iter().map(|b| if *b { 1.0 } else { 0.0 }).sum::<f64>() / (2.0 * (window as f64));
    data.push((0.5 + (i as f64), value));
  }

  let mut chart = ChartBuilder::on(&root)
    .x_label_area_size(35)
    .y_label_area_size(40)
    .margin(10)
    .build_cartesian_2d(range, 0.0f64..1.0)
    .unwrap();

  chart.configure_mesh()
    .x_desc("Time")
    .y_desc("Cumulative Success Rate")
    .axis_desc_style(("sans-serif", 15))
    .draw()
    .unwrap();

  chart.draw_series(PointSeries::of_element(data, 1, ShapeStyle::from(&RED).filled(),
    &|coord, size, style| { Circle::new(coord, size, style) }))
    .unwrap();
}
