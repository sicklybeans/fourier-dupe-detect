use indicatif::ProgressIterator;
use rand::seq::SliceRandom;
use rand::thread_rng;
use serde::Deserialize;
use std::collections::HashMap;
use std::fs::read_to_string;

use crate::signature::*;

#[derive(Deserialize)]
pub struct DupeKey {
  pub files: Vec<(usize, String)>,
  pub dupes: Vec<Vec<usize>>,
}

impl DupeKey {
  pub fn from_file(file: &str) -> Result<DupeKey, String> {
    let contents = read_to_string(file)
      .map_err(|e| format!("Couldnt ready key file: {:?}", e))?;
    serde_json::from_str(&contents)
      .map_err(|e| format!("Couldnt parse key file: {:?}", e))
  }

  pub fn get_ids(&self) -> Vec<usize> {
    self.files.iter().map(|(id, _)| id.clone()).collect()
  }
}

pub trait Learner {
  fn size(&self) -> usize;
  fn step(&mut self, input: &Vec<f32>, label: bool);
  fn run(&mut self, key: DupeKey, print_map: HashMap<usize, Fingerprint>) {
    let mut dupe_set: HashMap<usize, Vec<usize>> = HashMap::new();
    for dupe in key.dupes.iter() {
      for id in dupe.iter() {
        dupe_set.insert(*id, dupe.clone());
      }
    }

    // Create list of pairs to visit
    let mut image_pairs = Vec::new();
    for id1 in key.get_ids().into_iter() {
      for id2 in key.get_ids().into_iter() {
        image_pairs.push((id1, id2));
      }
    }

    // Randomize the order in which we visit them
    image_pairs.shuffle(&mut thread_rng());

    println!("Learning...");
    for (id1, id2) in image_pairs.iter().progress() {
      let label = if id1 == id2 {
        true
      } else {
        if let Some(dupes) = dupe_set.get(id1) {
          dupes.contains(id2)
        } else {
          false
        }
      };
      let input = (&print_map[id1] - &print_map[id2]).iter().map(|v| v.abs()).collect();
      self.step(&input, label)
    }
  }
}

pub struct LinearPerceptron {
  pub learning_rate: f32,
  pub bias: f32,
  pub weights: Vec<f32>,
}

impl LinearPerceptron {
  pub fn new(learning_rate: f32, bias: f32, size: usize) -> LinearPerceptron {
    LinearPerceptron {
      learning_rate,
      bias,
      weights: vec![0.0; size],
    }
  }
}

impl Learner for LinearPerceptron {

  fn size(&self) -> usize {
    self.weights.len()
  }

  fn step(&mut self, input: &Vec<f32>, label: bool) {
    let guess = self.weights.iter().zip(input.iter()).map(|(xi, wi)| xi * wi + self.bias).sum::<f32>() > 0.0;
    if guess != label {
      println!("Wrong!");
      for i in 0..self.weights.len() {
        if label {
          self.weights[i] += self.learning_rate * input[i]
        } else {
          self.weights[i] -= self.learning_rate * input[i]
        }
      }
    } else {
      println!("Right!");
    }
  }

}


pub fn learn(key: DupeKey, learning_rate: f32, bias: f32, cache_dir: Option<&str>) {

  let options = FingerprintOptions {
    channel_size: 100,
    channel_cutoff: 50
  };

  let mut print_map: HashMap<usize, Fingerprint> = HashMap::new();
  if let Some(dir) = cache_dir {
    println!("Preparing images and computing coefficients...");
    for (id, file) in key.files.iter().progress() {
      print_map.insert(*id, Fingerprint::load_or_compute(*id, file, dir, options.clone()).unwrap());
    }
  } else {
    let mut images: HashMap<usize, ReadyImage> = HashMap::new();
    println!("Preparing images...");
    for (id, file) in key.files.iter().progress() {
      images.insert(*id, ReadyImage::from_file(&file, options.clone()).unwrap());
    }

    println!("Computing coefficients...");
    for (id, image) in images.into_iter().progress() {
      print_map.insert(id, Fingerprint::compute(&image));
    }
  }

  let mut learner = LinearPerceptron::new(learning_rate, bias, options.num_components());
  learner.run(key, print_map);

}


