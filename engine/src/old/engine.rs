#[allow(dead_code)]
use std::fs::File;
use std::path::Path;
use image::{RgbImage, DynamicImage};
use image::imageops::FilterType;
use image::io::Reader as ImageReader;
use std::ops;
use std::fs::read_to_string;
use std::io::Write;
use serde::{Deserialize, Serialize};


#[derive(Clone, Serialize, Deserialize)]
pub struct FingerprintOptions {
  pub channel_size: u32,
  pub channel_cutoff: u32,
}

impl FingerprintOptions {
  pub fn num_pixels(&self) -> usize {
    return (self.channel_size * self.channel_size) as usize;
  }
  pub fn num_components(&self) -> usize {
    return self.num_pixels() * 3 * 2;
  }

}

pub struct ReadyImage {
  pub options: FingerprintOptions,
  resized: RgbImage,
  true_width: u32,
  true_height: u32,
  offset_x: u32,
  offset_y: u32,
}

impl ReadyImage {

  pub fn load_or_compute(id: usize, source: &str, dirname: &str, options: FingerprintOptions) -> Result<ReadyImage, String> {
    let saved_path = format!("{}/{}.png", dirname, id);
    if Path::new(&saved_path).exists() {
      ReadyImage::from_sized_file(&saved_path, options)
    } else {
      let image = ReadyImage::from_file(source, options)?;
      image.resized.save(&saved_path)
        .map_err(|_| format!("Could not save result of computed ready image"))?;
      Ok(image)
    }
  }

  fn shitty_val(&self, x: u32, y: u32) -> [f32; 3] {
    if x < self.offset_x || x >= self.true_width + self.offset_x {
      return [0.0; 3];
    }
    if y < self.offset_y || y >= self.true_height + self.offset_y {
      return [0.0; 3];
    }
    let p = self.resized.get_pixel(x - self.offset_x, y - self.offset_y).0;
    [p[0] as f32 / 256.0, p[1] as f32 / 256.0, p[2] as f32 / 256.0]
  }

  pub fn val(&self, x: u32, y: u32) -> [f32; 3] {
    self.shitty_val(x, y)
  }

  fn from_resized(resized: RgbImage, options: FingerprintOptions) -> ReadyImage {
    let true_width = resized.width();
    let true_height = resized.height();
    if true_width != options.channel_size && true_height != options.channel_size {
      panic!("Image was not in the expected format")
    }
    let offset_x = if true_width < options.channel_size {
      (options.channel_size - true_width) / 2
    } else {
      0
    };
    let offset_y = if true_height < options.channel_size {
      (options.channel_size - true_height) / 2
    } else {
      0
    };

    ReadyImage {
      resized,
      options,
      true_width,
      true_height,
      offset_x,
      offset_y,
    }
  }

  pub fn from_dynamic(img: &DynamicImage, options: FingerprintOptions) -> ReadyImage {
    ReadyImage::from_resized(img
      .resize(options.channel_size, options.channel_size, FilterType::Triangle)
      .into_rgb8(), options)
  }

  pub fn from_sized_file(fname: &str, options: FingerprintOptions) -> Result<ReadyImage, String> {
    match ImageReader::open(fname)
      .map_err(|e| format!("Couldnt open image file '{}' ({:?})", fname, e))?
      .decode()
      .map_err(|e| format!("Couldnt decode image file '{}' ({:?})", fname, e))? {
        DynamicImage::ImageRgb8(image) => Ok(ReadyImage::from_resized(image, options)),
        _ => Err(format!("Image was not in the proper format"))
      }
  }

  pub fn from_rgb_image(img: RgbImage, options: FingerprintOptions) -> ReadyImage {
    ReadyImage::from_dynamic(&DynamicImage::ImageRgb8(img), options)
  }

  pub fn from_file(fname: &str, options: FingerprintOptions) -> Result<ReadyImage, String> {
    ImageReader::open(fname)
      .map_err(|e| format!("Couldnt open image file '{}' ({:?})", fname, e))?
      .decode()
      .map_err(|e| format!("Couldnt decode image file '{}' ({:?})", fname, e))
      .map(|img| ReadyImage::from_dynamic(&img, options))
  }
}

#[derive(Deserialize, Serialize)]
pub struct Fingerprint {
  pub options: FingerprintOptions,
  pub real: [Vec<f32>; 3],
  pub imag: [Vec<f32>; 3],
}

impl Fingerprint {

  pub fn load_or_compute(id: usize, source: &str, dirname: &str, options: FingerprintOptions) -> Result<Fingerprint, String> {
    let saved_path = format!("{}/{}.finger", dirname, id);
    if Path::new(&saved_path).exists() {
      let contents = read_to_string(&saved_path)
        .map_err(|e| format!("Couldnt read fingerprint file: {:?}", e))?;
      serde_json::from_str(&contents)
        .map_err(|e| format!("Couldnt parse fingerprint file: {:?}", e))
    } else {
      let ready_image = ReadyImage::load_or_compute(id, source, dirname, options.clone())?;
      let result = Fingerprint::compute(&ready_image);
      let encoded = serde_json::to_vec(&result)
        .map_err(|e| format!("Couldnt encode fingerprint file: {:?}", e))?;
      let mut saved_file = File::create(&saved_path)
        .map_err(|e| format!("Couldnt save fingerprint file: {:?}", e))?;
      saved_file.write_all(&encoded)
        .map_err(|e| format!("Couldnt write fingerprint file: {:?}", e))?;
      Ok(result)
    }
  }

  pub fn compute(img: &ReadyImage) -> Fingerprint {
    let n = img.options.channel_size;
    let neff = img.options.channel_cutoff;

    // Initialize real and imaginary components of fourier coefficients for each color channel
    let mut re: [Vec<f32>; 3] = [vec![0.0; (n * n) as usize], vec![0.0; (n * n) as usize], vec![0.0; (n * n) as usize]];
    let mut im: [Vec<f32>; 3] = [vec![0.0; (n * n) as usize], vec![0.0; (n * n) as usize], vec![0.0; (n * n) as usize]];

    for kx in 0..neff {
      for ky in 0..neff {

        let factor_kx = 2.0 * 3.14159 / (n as f32) * (kx as f32);
        let factor_ky = 2.0 * 3.14159 / (n as f32) * (kx as f32);
        let ind = (kx * n + ky) as usize;

        for x in 0..n {
          for y in 0..n {
            let [fr, fg, fb] = img.val(x, y);
            let arg = factor_kx * x as f32 + factor_ky * y as f32;
            let real = arg.cos();
            let imag = -arg.cos();
            re[0][ind] += fr * real;
            im[0][ind] += fr * imag;
            re[1][ind] += fg * real;
            im[1][ind] += fg * imag;
            re[2][ind] += fb * real;
            im[2][ind] += fb * imag;
          }
        }
      }
    }
    Fingerprint {
      options: img.options.clone(),
      real: re,
      imag: im
    }
  }

}

impl ops::Sub<&Fingerprint> for &Fingerprint {
  type Output = Vec<f32>;

  fn sub(self, rhs: &Fingerprint) -> Vec<f32> {
    let color_size = self.options.num_pixels();
    let domain_size = 3 * color_size;
    let full_size = 2 * domain_size;

    let mut result = vec![0.0; full_size];

    for i in 0..color_size {
      result[0 * color_size + i] = self.real[0][i] - rhs.real[0][i];
      result[1 * color_size + i] = self.real[1][i] - rhs.real[1][i];
      result[2 * color_size + i] = self.real[2][i] - rhs.real[2][i];
      result[domain_size + 0 * color_size + i] = self.imag[0][i] - rhs.imag[0][i];
      result[domain_size + 1 * color_size + i] = self.imag[1][i] - rhs.imag[1][i];
      result[domain_size + 2 * color_size + i] = self.imag[2][i] - rhs.imag[2][i];
    }
    result
  }

}
