use sb_rust_library::bar::get_progress_bar;
use std::collections::HashMap;
use std::fmt::Debug;
use std::fs;
use std::path::Path;

use crate::labeled_files::LabeledFiles;
use crate::resource::*;

pub enum Error<T, U, V> {
  CacheLoadError(ResourceId, String, T),
  CacheSaveError(ResourceId, String, U),
  ComputeError(String, V),
}

pub trait Object: CacheableResource {
  type ComputeError: Debug;

  fn new(id: ResourceId, options: Self::Options, filename: &str) -> Result<Self, Self::ComputeError>;

  fn from_stored_or_compute(id: ResourceId, options: Self::Options, filename: &str, cache_dir: &str) -> Result<Self, Error<Self::LoadError, Self::SaveError, Self::ComputeError>> {
    use Error::*;
    if Self::has_cache_file(id, &options, cache_dir) {
      Self::load_cache(id, options, cache_dir)
        .map_err(|e| CacheLoadError(id, cache_dir.to_string(), e))
    } else {
      Self::new(id, options, filename)
        .map_err(|e| ComputeError(filename.to_string(), e))?
        .save_cache(cache_dir)
        .map_err(|e| CacheSaveError(id, cache_dir.to_string(), e))
    }
  }

  fn load_set(labeled_files: &LabeledFiles, options: &Self::Options, cache_dir: &str, verbose: bool) -> Result<ObjectSet<Self>, String> {
    // Create directory to store processed inputs in for cacheing
    if !Path::new(cache_dir).exists() {
      if let Err(e) = fs::create_dir(cache_dir) {
        eprintln!("Couldnt create cache directory for input set: {} ({:?})", cache_dir, e);
        panic!()
      }
    }

    // Nice progress bar output
    let bar = get_progress_bar("Preparing input files", labeled_files.files.len() as u64, verbose);

    // For each image id, generate the input
    let mut inputs = HashMap::new();
    for (id, file) in labeled_files.files.iter() {
      let input = Self::from_stored_or_compute(*id, options.clone(), &file, cache_dir)?;
      inputs.insert(*id, input);
      bar.inc(1);
    }
    bar.finish_and_clear();
    Ok(inputs)
  }
}

pub type ObjectSet<T> = HashMap<ResourceId, T>;

impl<T, U, V> From<Error<T, U, V>> for String
where T: Debug, U: Debug, V: Debug {
  fn from(err: Error<T, U, V>) -> String {
    use Error::*;
    match err {
      CacheLoadError(id, cache_dir, e) => format!("
        Unabled to read cached input file (id={}, cache_dir={})
        Cause: {:?}", id, cache_dir, e),
      CacheSaveError(id, cache_dir, e) => format!("
          Unabled to save cached input file (id={}, cache_dir={})
          Cause: {:?}", id, cache_dir, e),
      ComputeError(file, e) => format!("
          Unabled to generate input file for '{}'
          Cause: {:?}", file, e),
    }
  }
}
