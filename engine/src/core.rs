use image::{
  io::Reader as ImageReader,
  DynamicImage,
  ImageError,
};

use std::path::Path;

pub type ImageId = usize;

//////////////////////// Image loading stuff ///////////////////////////////////

#[derive(Debug)]
pub enum LoadError {
  NotFoundError,
  ReadError(String),
  ImageError(String),
}

pub fn load_image_file(filename: &str) -> Result<DynamicImage, LoadError> {
  if !Path::new(filename).exists() {
    Err(LoadError::NotFoundError)
  } else {
    ImageReader::open(filename)?
      .decode()
      .map_err(|e| e.into())
  }
}

impl From<std::io::Error> for LoadError {
  fn from(e: std::io::Error) -> LoadError {
    eprintln!("Error reading image: {:?}", e);
    LoadError::ReadError(format!("{:?}", e))
  }
}


impl From<ImageError> for LoadError {
  fn from(e: ImageError) -> LoadError {
    eprintln!("Error loading image: {:?}", e);
    LoadError::ImageError(format!("{:?}", e))
  }
}

