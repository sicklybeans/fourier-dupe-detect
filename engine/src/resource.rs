use std::fs;
use serde::Serialize;
use serde::de::DeserializeOwned;
use std::fmt::Debug;
use std::io::Write;

use crate::core::LoadError;

pub type ResourceId = usize;

pub trait Resource {
  type Options: Clone;

  fn get_id(&self) -> ResourceId;

  fn get_options(&self) -> &Self::Options;

  fn canonical_name(id: ResourceId, options: &Self::Options) -> String;

}

pub trait CacheableResource: Sized + Resource {
  type LoadError: Debug;
  type SaveError: Debug;

  fn cache_file(id: ResourceId, options: &Self::Options, cache_dir: &str) -> String {
    format!("{}/{}", cache_dir, Self::canonical_name(id, options))
  }

  fn has_cache_file(id: ResourceId, options: &Self::Options, cache_dir: &str) -> bool {
    std::path::Path::new(&Self::cache_file(id, options, cache_dir)).exists()
  }

  fn get_cache_file(&self, cache_dir: &str) -> String {
    Self::cache_file(self.get_id(), self.get_options(), cache_dir)
  }

  fn save_cache(self, cache_dir: &str) -> Result<Self, Self::SaveError>;

  fn load_cache(id: ResourceId, options: Self::Options, cache_dir: &str) -> Result<Self, Self::LoadError>;

}

pub trait SerializedResource: Sized + Resource + Serialize + DeserializeOwned {}

#[derive(Debug)]
pub enum SaveSerializeError {
  EncodeError(serde_json::Error),
  SaveError(std::io::Error),
}

impl<T> CacheableResource for T where T: SerializedResource {
  type LoadError = LoadError;
  type SaveError = SaveSerializeError;

  fn save_cache(self, cache_dir: &str) -> Result<Self, Self::SaveError> {
    let path = self.get_cache_file(cache_dir);
    let encoded = serde_json::to_vec(&self)
      .map_err(|e| SaveSerializeError::EncodeError(e))?;
    fs::File::create(&path)
      .map_err(|e| SaveSerializeError::SaveError(e))?
      .write_all(&encoded)
      .map_err(|e| SaveSerializeError::SaveError(e))
      .map(|_| self)
  }

  fn load_cache(id: ResourceId, options: Self::Options, cache_dir: &str) -> Result<Self, LoadError> {
    let path = Self::cache_file(id, &options, cache_dir);
    let contents = fs::read_to_string(&path)
      .map_err(|e| LoadError::ReadError(format!("Couldnt read file: {:?}", e)))?;
    serde_json::from_str(&contents)
      .map_err(|e| LoadError::ReadError(format!("Couldnt parse file: {:?}", e)))
  }
}
