mod representation;
mod signature;
mod fourier_representation;
mod fourier_signatures;

pub use representation::{
  Error,
  Representation,
  RepresentationSet,
};
pub use signature::*;
pub use fourier_representation::{
  FourierRepresentation,
  FourierRepresentationOptions,
};
pub use fourier_signatures::*;
