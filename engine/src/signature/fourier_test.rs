// use image::{Rgb, RgbImage};
// use num::complex::Complex;

// use super::fourier::*;


// fn reverse_ft(sig: &FourierRepresentation) -> Vec<Vec<[f64; 3]>> {
//   let n = sig.dims;
//   let norm = 1.0 / ((n * n) as f64);
//   (0..n).map(|y| {
//     (0..n).map(|x| {

//       let factor_x = 2.0 * 3.14159 / (n as f64) * (x as f64);
//       let factor_y = 2.0 * 3.14159 / (n as f64) * (y as f64);
//       let mut sum = [Complex::new(0.0, 0.0), Complex::new(0.0, 0.0), Complex::new(0.0, 0.0)];

//       for ky in sig.nonzero_ky_range() {
//         for kx in sig.nonzero_kx_range() {
//           let vals = sig.coefficient(kx, ky);
//           for i in 0..3 {
//             let f = vals[i];
//             let a = Complex::new(0.0, factor_x * (kx as f64) + factor_y * (ky as f64)).exp();
//             sum[i] += Complex::new(f.x * a.re - f.y * a.im, f.x * a.im + f.y * a.re);
//           }
//         }
//       }
//       [norm * sum[0].re, norm * sum[1].re, norm * sum[2].re]
//     }).collect()
//   }).collect()
// }

// fn pixels_to_img(values: Vec<Vec<[u8; 3]>>, scale_factor: u32) -> RgbImage {
//   let n_x = values.len() as u32;
//   let n_y = values[0].len() as u32;
//   let mut img = RgbImage::new(n_x, n_y);

//   for x in 0..n_x {
//     for y in 0..n_y {
//       for xi in 0..scale_factor {
//         for yi in 0..scale_factor {
//           img.put_pixel(x * scale_factor + xi, y * scale_factor + yi, Rgb(values[y as usize][x as usize]));
//         }
//       }
//     }
//   }
//   img
// }

// fn spatial_rep_to_pixels(spatial_representation: Vec<Vec<[f64; 3]>>) -> Vec<Vec<[u8; 3]>> {
//   spatial_representation.into_iter()
//     .map(|values| values.into_iter()
//       .map(|v| {
//         [(v[0] * 256.0) as u8, (v[1] * 256.0) as u8, (v[2] * 256.0) as u8]
//       })
//       .collect()
//     )
//     .collect()
// }

