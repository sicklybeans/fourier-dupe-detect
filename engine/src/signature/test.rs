#![allow(dead_code)]
use sb_rust_library::bar::get_progress_bar;
use plotters::prelude::*;

use crate::labeled_files::*;
use crate::input::*;
use crate::resource::*;
use super::*;

fn plot_zero_mode(output_dir: &str, labeled_files: LabeledFiles, _input_set: ObjectSet<ResizedImage>, representations: RepresentationSet<FourierRepresentation>) -> Result<(), String> {

  let dupe_ids = labeled_files.get_dupe_pairs(None);
  let regu_ids = labeled_files.get_regular_pairs(dupe_ids.len());

  let x_label = "Real(F(0,0))";
  let y_label = "Real(F(1,1))";

  for c in 0..3 {
    let zero_mode_deltas_dupes: Vec<(f64, f64)> = dupe_ids.iter()
      .map(|(id1, id2)| (&representations[id1], &representations[id2]))
      .map(|(rep1, rep2)| {
        let fk0 = rep1.coefficient(0, 0)[c] - rep2.coefficient(0, 0)[c];
        let fk1 = rep1.coefficient(1, 1)[c] - rep2.coefficient(1, 1)[c];
        (fk0.x, fk1.x)
      })
      .collect();

    let zero_mode_deltas_tests: Vec<(f64, f64)> = regu_ids.iter()
      .map(|(id1, id2)| (&representations[id1], &representations[id2]))
      .map(|(rep1, rep2)| {
        let fk0 = rep1.coefficient(0, 0)[c] - rep2.coefficient(0, 0)[c];
        let fk1 = rep1.coefficient(1, 1)[c] - rep2.coefficient(1, 1)[c];
        (fk0.x, fk1.x)
      })
      .collect();

    let fname = format!("{}/zero-mode-delta-{}.png", output_dir, c);
    let root = BitMapBackend::new(&fname, (640, 480)).into_drawing_area();
    root.fill(&WHITE).unwrap();
    let mut chart = ChartBuilder::on(&root)
      .x_label_area_size(35)
      .y_label_area_size(40)
      .margin(10)
      .build_cartesian_2d(-1000f64..1000.0, -300.0f64..300.0)
      .unwrap();

    chart.configure_mesh()
      .x_desc(x_label)
      .y_desc(y_label)
      .axis_desc_style(("sans-serif", 15))
      .draw()
      .unwrap();

    chart.draw_series(PointSeries::of_element(zero_mode_deltas_dupes, 2, ShapeStyle::from(&RED).filled(),
      &|coord, size, style| {
        Circle::new(coord, size, style)
      }))
      .unwrap();
    chart.draw_series(PointSeries::of_element(zero_mode_deltas_tests, 2, ShapeStyle::from(&BLUE).filled(),
      &|coord, size, style| {
        Circle::new(coord, size, style)
      }))
      .unwrap();
  }
  Ok(())
}

fn plot_first_mode(output_dir: &str, labeled_files: LabeledFiles, _input_set: ObjectSet<ResizedImage>, representations: RepresentationSet<FourierRepresentation>) -> Result<(), String> {

  let dupe_ids = labeled_files.get_dupe_pairs(None);
  let regu_ids = labeled_files.get_regular_pairs(dupe_ids.len());

  let x_label = "Real(F(1,1))";
  let y_label = "Imag(F(1,1))";

  for c in 0..3 {
    let dupe_deltas: Vec<(f64, f64)> = dupe_ids.iter()
      .map(|(id1, id2)| (&representations[id1], &representations[id2]))
      .map(|(rep1, rep2)| {
        let d = rep1.coefficient(1, 1)[c] - rep2.coefficient(1, 1)[c];
        (d.x, d.y)
      })
      .collect();

    let test_deltas: Vec<(f64, f64)> = regu_ids.iter()
      .map(|(id1, id2)| (&representations[id1], &representations[id2]))
      .map(|(rep1, rep2)| {
        let d = rep1.coefficient(1, 1)[c] - rep2.coefficient(1, 1)[c];
        (d.x, d.y)
      })
      .collect();

    let fname = format!("{}/first-mode-delta-{}.png", output_dir, c);
    let root = BitMapBackend::new(&fname, (640, 480)).into_drawing_area();
    root.fill(&WHITE).unwrap();
    let mut chart = ChartBuilder::on(&root)
      .x_label_area_size(35)
      .y_label_area_size(40)
      .margin(10)
      .build_cartesian_2d(-1000f64..1000.0, -300.0f64..300.0)
      .unwrap();

    chart.configure_mesh()
      .x_desc(x_label)
      .y_desc(y_label)
      .axis_desc_style(("sans-serif", 15))
      .draw()
      .unwrap();

    chart.draw_series(PointSeries::of_element(dupe_deltas, 2, ShapeStyle::from(&RED).filled(),
      &|coord, size, style| {
        Circle::new(coord, size, style)
      }))
      .unwrap();
    chart.draw_series(PointSeries::of_element(test_deltas, 2, ShapeStyle::from(&BLUE).filled(),
      &|coord, size, style| {
        Circle::new(coord, size, style)
      }))
      .unwrap();
  }
  Ok(())
}

fn make_plot<F>(
  representations: &RepresentationSet<FourierRepresentation>,
  dupes: &Vec<(ResourceId, ResourceId)>,
  tests: &Vec<(ResourceId, ResourceId)>,
  _xlabel: &str,
  _ylabel: &str,
  xrange: std::ops::Range<f64>,
  yrange: std::ops::Range<f64>,
  output_name: &str,
  title: &str,
  data_fn: &F) -> Result<(), ()>
where F: Fn((&FourierRepresentation, &FourierRepresentation)) -> (f64, f64) {
  let dupe_values: Vec<(f64, f64)> = dupes.iter()
    .map(|(id1, id2)| (&representations[id1], &representations[id2]))
    .map(data_fn)
    .collect();
  let test_values: Vec<(f64, f64)> = tests.iter()
    .map(|(id1, id2)| (&representations[id1], &representations[id2]))
    .map(data_fn)
    .collect();

  let mut root = BitMapBackend::new(output_name, (640, 480)).into_drawing_area();
  root.fill(&WHITE).unwrap();
  root = root.titled(title, ("Noto Sans", 40)).unwrap();
  let mut chart = ChartBuilder::on(&root)
    .x_label_area_size(35)
    .y_label_area_size(40)
    .margin(10)
    .build_cartesian_2d(xrange, yrange)
    .unwrap();

  // chart.configure_mesh().draw().unwrap();
  //   .x_desc(x_label)
  //   .y_desc(y_label)
  //   .axis_desc_style(("sans-serif", 15))
  //   .draw() {
  //   println!("Error: {:?}", e);
  //   return Err(String::from("yo something went wrong"))
  // }

  chart.draw_series(PointSeries::of_element(dupe_values, 2, ShapeStyle::from(&RED).filled(),
    &|coord, size, style| { Circle::new(coord, size, style) }))
    .unwrap();
  chart.draw_series(PointSeries::of_element(test_values, 2, ShapeStyle::from(&BLUE).filled(),
    &|coord, size, style| { Circle::new(coord, size, style) }))
    .unwrap();
  Ok(())
}

fn plot_many_things(output_dir: &str, labeled_files: LabeledFiles, _input_set: ObjectSet<ResizedImage>, representations: RepresentationSet<FourierRepresentation>) -> Result<(), String> {

  let dupe_ids = labeled_files.get_dupe_pairs(None);
  let test_ids = labeled_files.get_regular_pairs(dupe_ids.len());

  for kx in 0..3 {
    for ky in 0..3 {
      for color in 0..3 {
        make_plot(&representations, &dupe_ids, &test_ids, "real", "imag", -500.0..500.0, -500.0..500.0,
          &format!("{}/many-kx{}-ky{}-{}.png", output_dir, kx, ky, color),
          &format!("F({}, {}) channel {}", kx, ky, color),
          &|(rep1, rep2): (&FourierRepresentation, &FourierRepresentation)| {
            let d = rep1.coefficient(kx, ky)[color] - rep2.coefficient(kx, ky)[color];
            (d.x, d.y)
          }).unwrap();
      }
    }
  }

  Ok(())
}

fn plot_splitter_one(output_dir: &str, labeled_files: LabeledFiles, _input_set: ObjectSet<ResizedImage>, representations: RepresentationSet<FourierRepresentation>) -> Result<(), String> {

  let dupe_ids = labeled_files.get_dupe_pairs(None);
  let test_ids = labeled_files.get_regular_pairs(100000);

  make_plot(&representations, &dupe_ids, &test_ids, "Log(|F(1,1)|^2)", "Log(|F(2,2)|^2)", -0.0..20.0, -0.0..20.0,
    &format!("{}/splitter-k1-k2.png", output_dir),
    &format!("First two modes"),
    &|(rep1, rep2): (&FourierRepresentation, &FourierRepresentation)| {
      let mut d1: f64 = (0..3)
        .map(|color| rep1.coefficient(1, 1)[color] - rep2.coefficient(1, 1)[color])
        .map(|delta| delta.norm2())
        .sum();
      let mut d2: f64 = (0..3)
        .map(|color| rep1.coefficient(2, 2)[color] - rep2.coefficient(2, 2)[color])
        .map(|delta| delta.norm2())
        .sum();
      if d1 == 0.0 {
        d1 = 0.0001;
      }
      if d2 == 0.0 {
        d2 = 0.0001;
      }
      (d1.ln(), d2.ln())
    })
    .unwrap();

  Ok(())
}

fn plot_lowest_norms(output_dir: &str, labeled_files: LabeledFiles, _input_set: ObjectSet<ResizedImage>, representations: RepresentationSet<FourierRepresentation>) -> Result<(), String> {

  let dupe_ids = labeled_files.get_dupe_pairs(None);
  let regu_ids = labeled_files.get_regular_pairs(dupe_ids.len());

  let _x_label = "|dF(0,0)|";
  let _y_label = "|dF(1,1)|";
  let name = "lowest-norms";

  for c in 0..3 {
    let dupe_deltas: Vec<(f64, f64)> = dupe_ids.iter()
      .map(|(id1, id2)| (&representations[id1], &representations[id2]))
      .map(|(rep1, rep2)| {
        let d0 = (rep1.coefficient(0, 0)[c] - rep2.coefficient(0, 0)[c]).norm();
        let d1 = (rep1.coefficient(1, 1)[c] - rep2.coefficient(1, 1)[c]).norm();
        if d1 > 100.0 {
          println!("High delta dupe: {}, {}", rep1.id, rep2.id);
        }

        (d0, d1)
      })
      .collect();

    let test_deltas: Vec<(f64, f64)> = regu_ids.iter()
      .map(|(id1, id2)| (&representations[id1], &representations[id2]))
      .map(|(rep1, rep2)| {
        let d0 = (rep1.coefficient(0, 0)[c] - rep2.coefficient(0, 0)[c]).norm();
        let d1 = (rep1.coefficient(1, 1)[c] - rep2.coefficient(1, 1)[c]).norm();
        if d1 < 100.0 {
          println!("Low delta regular: {}, {}", rep1.id, rep2.id);
        }

        (d0, d1)
      })
      .collect();

    let fname = format!("{}/{}-{}.png", output_dir, name, c);
    let root = BitMapBackend::new(&fname, (640, 480)).into_drawing_area();
    root.fill(&WHITE).unwrap();
    let mut chart = ChartBuilder::on(&root)
      .x_label_area_size(35)
      .y_label_area_size(40)
      .margin(10)
      .build_cartesian_2d(-1000f64..1000.0, -300.0f64..300.0)
      .unwrap();

    // if let Err(e) = chart.configure_mesh()
    //   .x_desc(x_label)
    //   .y_desc(y_label)
    //   .axis_desc_style(("sans-serif", 15))
    //   .draw() {
    //   println!("Error: {:?}", e);
    //   return Err(String::from("yo something went wrong"))
    // }

    chart.draw_series(PointSeries::of_element(dupe_deltas, 2, ShapeStyle::from(&RED).filled(),
      &|coord, size, style| {
        Circle::new(coord, size, style)
      }))
      .unwrap();
    chart.draw_series(PointSeries::of_element(test_deltas, 2, ShapeStyle::from(&BLUE).filled(),
      &|coord, size, style| {
        Circle::new(coord, size, style)
      }))
      .unwrap();
  }
  Ok(())
}

fn test_recreate_images() -> Result<(), String> {
  let cache_dir = "./output/recreate-test";
  let key_file = "/home/sam/workspace/fourier-dupe-detect/input-files/recreate-test/key.json";
  let labeled_files = LabeledFiles::from_key(key_file)?;

  let obj_options = ResizedImageOptions { size: 100, };
  let rep_options = FourierRepresentationOptions { cutoff: 100, };

  let objs = ResizedImage::load_set(&labeled_files, &obj_options, cache_dir, true)?;
  let reps = FourierRepresentation::load_set(&objs, &rep_options, cache_dir, true)?;

  let bar = get_progress_bar("Reversing transform", labeled_files.files.len() as u64, true);
  for (id, sig) in reps.iter() {
    let img = fourier_test::image_from_representation(sig);
    let output_name = ResizedImage::cache_file(*id, &obj_options, cache_dir);
    img.save(format!("{}.recreated.png", output_name)).unwrap();
    bar.inc(1);
  }
  bar.finish_and_clear();

  // println!("Finished: {}", representations.keys().len());
  Ok(())
}

fn test_inner() -> Result<(), String> {
  let cache_dir = "./output/test-2";
  let key_file = "/home/sam/workspace/fourier-dupe-detect/input-files/ready/key.json";
  let labeled_files = LabeledFiles::from_key(key_file)?;

  let input_options = ResizedImageOptions { size: 100, };
  let representation_options = FourierRepresentationOptions { cutoff: 4, };

  let objs = ResizedImage::load_set(&labeled_files, &input_options, cache_dir, true)?;
  let reps = FourierRepresentation::load_set(&objs, &representation_options, cache_dir, true)?;

  plot_splitter_one(cache_dir, labeled_files, objs, reps).unwrap();

  // println!("Finished: {}", representations.keys().len());
  Ok(())
}