use super::representation::*;

pub trait Signature<R: Representation> {
  fn compute(&self, rep1: &R, rep2: &R) -> Vec<f64>;
  fn size(&self) -> usize;
}
